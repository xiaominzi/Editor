function getEvent(){
    if(window.event)    {return window.event;}
    func=getEvent.caller;
    while(func!=null){
        var arg0=func.arguments[0];
        if(arg0){
            if((arg0.constructor==Event || arg0.constructor ==MouseEvent
                || arg0.constructor==KeyboardEvent)
                ||(typeof(arg0)=="object" && arg0.preventDefault
                && arg0.stopPropagation)){
                return arg0;
            }
        }
        func=func.caller;
    }
    return null;
}
//阻止冒泡
function stopBubble()
{
    var e=getEvent();
    if(window.event){
        //e.returnValue=false;//阻止自身行为
        e.cancelBubble=true;//阻止冒泡
    }else if(e.preventDefault){
        //e.preventDefault();//阻止自身行为
        e.stopPropagation();//阻止冒泡
    }
}
function edwang(idid,imgsrc){
    var editor = new wangEditor(idid);
    // 上传图片
    editor.config.uploadImgUrl = imgsrc;
    editor.config.uploadImgFileName = 'file'

    editor.config.menus = [
        'bold',
        'underline',
        'italic',
        'strikethrough',
        'eraser',
        '|',
        'quote',
        'unorderlist',
        'orderlist',
        '|',
        'alignleft',
        'aligncenter',
        'alignright',
        '|',
        // 'link',
        // 'unlink',
        'table',
        'emotion',
        'red',
        'blue',
        'tree',
        '|',
        'imgtext',
        'imgtext50',
        'img',
        // 'insertcode',
        '|',
        'undo',
        'redo',
        'fullscreen'
    ];
    editor.config.emotions = {
        // 支持多组表情

        // 第一组，id叫做 'default'


        // 第二组，id叫做'weibo'
        'weibo': {
            title: '微博表情',
            data: [
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7a/shenshou_thumb.gif',
                    value: '[草泥马]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/60/horse2_thumb.gif',
                    value: '[神马]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/bc/fuyun_thumb.gif',
                    value: '[浮云]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c9/geili_thumb.gif',
                    value: '[给力]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/f2/wg_thumb.gif',
                    value: '[围观]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/70/vw_thumb.gif',
                    value: '[威武]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/6e/panda_thumb.gif',
                    value: '[熊猫]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/81/rabbit_thumb.gif',
                    value: '[兔子]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/bc/otm_thumb.gif',
                    value: '[奥特曼]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/15/j_thumb.gif',
                    value: '[囧]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/89/hufen_thumb.gif',
                    value: '[互粉]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/c4/liwu_thumb.gif',
                    value: '[礼物]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/ac/smilea_thumb.gif',
                    value: '[呵呵]'
                },
                {
                    icon: 'http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/0b/tootha_thumb.gif',
                    value: '[哈哈]'
                }
            ]
        }
        // 下面还可以继续，第三组、第四组、、、
    };
    var content2 = $('#'+idid).html();
    var create_C = $("<input type='hidden' value='"+content2+"' name='"+idid+"'>");//reply_content
    $('#'+idid).parents('form').append(create_C);
    // onchange 事件
    editor.onchange = function () {
        var contet = this.$txt.html();
        $('input[name="'+idid+'"]').val(contet);
    };
    editor.config.pasteFilter = false;
    editor.config.withCredentials = false;

    // editor.config.uploadImgFileName = 'myFileName';

    // var file = request.files['wangEditorH5File'];
    // var file = request.files['wangEditorFormFile'];
    // var file = request.files['wangEditorPasteFile'];
    // var file = request.files['wangEditorDragFile'];
    editor.create();
}
